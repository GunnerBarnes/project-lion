from django import forms
from reviews.models import Restaurant, Review
from localflavor.us.forms import USStateSelect


class RestaurantForm(forms.ModelForm):
	restaurant_name = forms.CharField(label='Name', widget=forms.TextInput(attrs={'class': 'input-wide', 'placeholder': 'Name'}))
	address = forms.CharField(widget=forms.TextInput(attrs={'class': 'input-wide', 'placeholder': 'Address'}))
	city = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'City'}))
	state = forms.CharField(widget=USStateSelect(), initial="NY")
	zipcode = forms.CharField(label='Zip Code', widget=forms.TextInput(attrs={'type': 'number', 'placeholder': 'Zip Code'}))
	website = forms.URLField(widget=forms.TextInput(attrs={'class': 'input-wide', 'placeholder': 'Website'}), required=False)
	phone = forms.CharField(widget=forms.TextInput(attrs={'type': 'tel', 'placeholder': 'Phone'}))
	class Meta:
		model = Restaurant
		fields = ['restaurant_name', 'address', 'city', 'state', 'zipcode', 'website', 'phone']

class ReviewForm(forms.ModelForm):
	review_text = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control no-resize-text-area', 'rows': 5, 'maxlength': 140, 'placeholder': 'Enter your review here (limit 140 chars)'}))
	class Meta:
		model = Review
		fields = ['review_text', 'rating']

class ContactForm(forms.Form):
    from_email = forms.EmailField(label='Your email', required=True)
    message = forms.CharField(widget=forms.Textarea(attrs={'class': 'no-resize-text-area'}))
