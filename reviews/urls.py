from django.conf.urls import patterns, url

from reviews import views

urlpatterns = patterns('',
	# ex: /about
	url(r'^about/$', views.about, name='about'),
	# ex: /contact
	url(r'^contact/$', views.contact, name='contact'),
	url(r'^thanks/$', views.thanks, name='thanks'),
	# ex: /; TODO: eventually, this will be the city landing page
	url(r'^$', views.index, name='index'),
	#ex : /add_restaurant
	url(r'^add_restaurant/$', views.add_restaurant, name='add_restaurant'),
	# ex: /restaurant/1
	url(r'^restaurant/(?P<restaurant_id>\d+)/$', views.restaurant, name='restaurant'),
	# ex: /restaurant/1/addreview
	url(r'^restaurant/(?P<restaurant_id>\d+)/addreview/$', views.add_review, name='addreview'),
	# ex: /restaurant/1/4/vote
	url(r'^restaurant/(?P<restaurant_id>\d+)/(?P<review_id>\d+)/favreview/$', views.fav_review, name='fav_review'),
	# ex: /restaurant/1/vote
	url(r'^restaurant/(?P<restaurant_id>\d+)/favrest/$', views.fav_restaurant, name='fav_restaurant')
)