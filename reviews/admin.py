from django.contrib import admin
from reviews.models import Review, Restaurant

class ReviewAdmin(admin.ModelAdmin):
	fields = ['restaurant', 'review_text', 'rating', 'created_by']
	list_display = ['restaurant', 'review_text', 'rating', 'created_by']
	list_filter = ['restaurant', 'rating']
	search_fields = ['restaurant']

admin.site.register(Review, ReviewAdmin)

class RestaurantAdmin(admin.ModelAdmin):
	fields = ['restaurant_name', 'address', 'city', 'state', 'zipcode', 'website', 'phone', 'published']
	list_display = ['restaurant_name', 'city', 'is_published']
	list_filter = ['state','city']
	search_fields = ['restaurant_name']

admin.site.register(Restaurant, RestaurantAdmin)