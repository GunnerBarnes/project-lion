import datetime
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import User


# Create your models here.

REVIEW_CHOICES = (
		(True, "Good"),
		(None, "Meh"),		
		(False, "Bad")
	)

class TimeStampedModel(models.Model):
	created = models.DateTimeField(auto_now_add=True, editable=False, default=timezone.now)
	modified = models.DateTimeField(auto_now=True, default=timezone.now)
	created_by = models.ForeignKey(User, default=1)

	class Meta:
		abstract = True

class Restaurant(TimeStampedModel):
	restaurant_name = models.CharField(max_length=200)
	address = models.CharField(max_length=200)
	city = models.CharField(max_length=200)
	state = models.CharField(max_length=2)
	zipcode = models.CharField(max_length=10)
	website = models.CharField(max_length=100, default=None, blank=True)
	phone = models.CharField(max_length=10, blank=True)
	published = models.DateTimeField(null=True, default=None, blank=True)
	upvotes = models.IntegerField(default=0)
	def __str__(self):
		return self.restaurant_name

	def was_published_recently(self):
		return self.published >= timezone.now() - datetime.timedelta(days=1)

	def is_published(self):
		return self.published is not None
	is_published.admin_order_field = 'published'
	is_published.boolean = True
	is_published.short_description = "Published?"

	def formatted_phone_number(self):
		return "%s%s%s-%s%s%s-%s%s%s%s" % tuple(self.phone)

class Review(TimeStampedModel):
	restaurant = models.ForeignKey(Restaurant)
	review_text = models.CharField(max_length=140)
	rating = models.NullBooleanField(choices=REVIEW_CHOICES, default=True)
	upvotes = models.IntegerField(default=0)
	downvotes = models.IntegerField(default=0)
	def __str__(self):
		return self.review_text

class Vote(TimeStampedModel):
	restaurant = models.ForeignKey(Restaurant, default=None)
	review = models.ForeignKey(Review, default=None, null=True)
