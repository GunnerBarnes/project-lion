// javascript file for upvoting

var options = {
	valueNames: [ 'rest-name', 'rest-city' ]
};

var restList = new List('restaurants', options);

var csrftoken = $.cookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


function favoriteReview(review_id, restaurant_id, element){
	var review_item = element.closest("li");
	var review_vote = $(review_item).find("div.voting");
	$.ajax({
	    url: "/restaurant/" + restaurant_id + "/" + review_id + "/favreview/",
	    type: "POST",
	    data: { 
	            	'review_id' : review_id,
	            	'restaurant_id' : restaurant_id
	            },
	    dataType: "html",
	    success: function(resp){
	        $(review_vote).replaceWith(resp);
	    }
	});
}

function favoriteRestaurant(restaurant_id, user_votes, element){
	var restaurant_item = element.closest("li");
	var restaurant_vote = $(restaurant_item).find("div.rest-voting");
	$.ajax({
		url: "/restaurant/" + restaurant_id + "/favrest/",
		type: "POST",
		data: { 'restaurant_id' : restaurant_id },
		dataType: "html",
		success: function(resp){
			$(restaurant_vote).replaceWith(resp);
		}
	});
}