# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0006_auto_20150221_1411'),
    ]

    operations = [
        migrations.AlterField(
            model_name='restaurant',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 21, 19, 56, 43, 688785, tzinfo=utc), auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='restaurant',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 21, 19, 56, 43, 689109, tzinfo=utc), auto_now=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='restaurant',
            name='published',
            field=models.DateTimeField(default=None, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='review',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 21, 19, 56, 43, 688785, tzinfo=utc), auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='review',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 21, 19, 56, 43, 689109, tzinfo=utc), auto_now=True),
            preserve_default=True,
        ),
    ]
