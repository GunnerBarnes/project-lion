# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0005_auto_20150221_1319'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='review',
            name='published',
        ),
        migrations.AlterField(
            model_name='restaurant',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 21, 14, 11, 24, 143477, tzinfo=utc), auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='restaurant',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 21, 14, 11, 24, 143825, tzinfo=utc), auto_now=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='review',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 21, 14, 11, 24, 143477, tzinfo=utc), auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='review',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 21, 14, 11, 24, 143825, tzinfo=utc), auto_now=True),
            preserve_default=True,
        ),
    ]
