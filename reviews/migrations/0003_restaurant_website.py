# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0002_auto_20150208_1330'),
    ]

    operations = [
        migrations.AddField(
            model_name='restaurant',
            name='website',
            field=models.CharField(max_length=100, blank=True),
            preserve_default=True,
        ),
    ]
