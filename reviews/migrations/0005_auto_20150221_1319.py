# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0004_restaurant_phone'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='restaurant',
            name='is_published',
        ),
        migrations.AddField(
            model_name='restaurant',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 21, 13, 19, 39, 59875, tzinfo=utc), auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='restaurant',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 21, 13, 19, 39, 60347, tzinfo=utc), auto_now=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='restaurant',
            name='published',
            field=models.DateTimeField(default=None, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='review',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 21, 13, 19, 39, 59875, tzinfo=utc), auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='review',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2015, 2, 21, 13, 19, 39, 60347, tzinfo=utc), auto_now=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='review',
            name='published',
            field=models.DateTimeField(default=None, null=True),
            preserve_default=True,
        ),
    ]
