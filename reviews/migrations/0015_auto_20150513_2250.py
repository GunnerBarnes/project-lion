# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0014_restaurant_upvotes'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vote',
            name='review',
            field=models.ForeignKey(default=None, to='reviews.Review', null=True),
            preserve_default=True,
        ),
    ]
