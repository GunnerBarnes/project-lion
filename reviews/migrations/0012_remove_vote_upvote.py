# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0011_auto_20150321_1923'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='vote',
            name='upvote',
        ),
    ]
