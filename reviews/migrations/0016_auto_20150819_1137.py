# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reviews', '0015_auto_20150513_2250'),
    ]

    operations = [
        migrations.AlterField(
            model_name='restaurant',
            name='website',
            field=models.CharField(default=None, max_length=100, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='review',
            name='rating',
            field=models.NullBooleanField(default=True, choices=[(b'', b'-select-'), (True, b'Good'), (None, b'Average'), (False, b'Bad')]),
            preserve_default=True,
        ),
    ]
