from django.shortcuts import render, get_object_or_404, get_list_or_404
from reviews.forms import RestaurantForm, ReviewForm, ContactForm
from django.core.urlresolvers import reverse
from reviews.models import Restaurant, Review, Vote
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect
from django.core.mail import EmailMessage, BadHeaderError


def about(request):
	return render(request, 'reviews/about.html')

# def contact(request):
# 	return render(request, 'reviews/contact.html')

def thanks(request):
	return render(request, 'reviews/thanks.html')

def index(request):
	restaurant_listing = get_restaurant_listing(request)
	context = { 
	 'restaurant_list': restaurant_listing['restaurant_list'],
	 'user_votes': restaurant_listing['user_votes'],
	 'review_totals' : restaurant_listing['review_totals']
	}
	return render(request, 'reviews/index.html', context)

def get_restaurant_votes(request):
	 if request.user.id == None:
	 	user_votes = None
	 else:
	 	user_votes = Vote.objects.filter(review=None, created_by=request.user).values_list('restaurant__id', flat=True)
	 return user_votes

def get_restaurant_user_votes(request, restaurant):
	if request.user.id == None:
	 	user_votes = None
	else:
		user_votes = Vote.objects.filter(review=None, created_by=request.user).values_list('restaurant__id', flat=True)
	return user_votes

def get_restaurant_listing(request):
	restaurant_list = Restaurant.objects.order_by('restaurant_name')
	user_votes = get_restaurant_votes(request)
	review_totals = {}
	for restaurant in restaurant_list:
		review_count = Review.objects.filter(restaurant=restaurant).count()
		positive_review_count = Review.objects.filter(restaurant=restaurant, rating=True).distinct('created_by').count()
		negative_review_count = Review.objects.filter(restaurant=restaurant, rating=False).distinct('created_by').count()
		mediocre_review_count = Review.objects.filter(restaurant=restaurant, rating=None).distinct('created_by').count()
		review_totals[restaurant.id] = { 'positive_count' : positive_review_count, 'negative_count' : negative_review_count, 'mediocre_count' : mediocre_review_count }

	restaurant_listing = {
		'restaurant_list' : restaurant_list,
		'user_votes' : user_votes,
		'review_totals' : review_totals
	}
	return restaurant_listing
	

def restaurant(request, restaurant_id):
	restaurant = get_object_or_404(Restaurant, pk=restaurant_id)
	all_reviews = get_reviews(request, restaurant)
	return render(request, 'reviews/restaurant.html', 
		{
			'restaurant' : all_reviews['restaurant'],
			'positive_reviews' : all_reviews['positive_reviews'],
			'negative_reviews' : all_reviews['negative_reviews'],
			'mediocre_reviews' : all_reviews['mediocre_reviews'],
			'user_votes' : all_reviews['user_votes']
		})

def get_reviews(request, restaurant):
	positive_reviews = Review.objects.filter(restaurant=restaurant, rating=True).order_by('-upvotes')
	negative_reviews = Review.objects.filter(restaurant=restaurant, rating=False).order_by('-upvotes')
	mediocre_reviews = Review.objects.filter(restaurant=restaurant, rating=None).order_by('-upvotes')
	
	if request.user.id == None:
		user_votes = None
	else:
		user_votes = Vote.objects.filter(restaurant=restaurant, created_by=request.user).values_list('review__id', flat=True)
	all_reviews = {
			'restaurant' : restaurant, 
			'positive_reviews' : positive_reviews,
			'negative_reviews' : negative_reviews,
			'mediocre_reviews' : mediocre_reviews,
			'user_votes' : user_votes
		}
	return all_reviews

def fix_website_url(url):
	correct_url = ""
	if not url:
		return correct_url
	if url.startswith("www"):
		correct_url = "http://" + url
	else:
		correct_url = "http://www." + url
	return correct_url

def add_restaurant(request):
    # A HTTP POST?
    if request.method == 'POST':
        form = RestaurantForm(request.POST)

        # Have we been provided with a valid form?
        if form.is_valid():
            # Save the new restaurant to the database.
            new_restaurant = form.save(commit=False)
            new_restaurant.created_by = request.user
            if not new_restaurant.website.startswith("http"):
            	new_restaurant.website = fix_website_url(new_restaurant.website)
            new_restaurant.save()

            # Now call the index() view.
            # The user will be shown the homepage.
            return HttpResponseRedirect(reverse('reviews:index'))
        else:
            # The supplied form contained errors - just print them to the terminal.
            print form.errors
    else:
        # If the request was not a POST, display the form to enter details.
        form = RestaurantForm()

    # Bad form (or form details), no form supplied...
    # Render the form with error messages (if any).
    return render(request, 'reviews/addrestaurant.html', {'form': form})

def add_review(request, restaurant_id):
	restaurant = get_object_or_404(Restaurant, pk=restaurant_id)
	if request.method == 'POST':
		form = ReviewForm(request.POST)

		if form.is_valid():
			new_review = form.save(commit=False)
			new_review.created_by = request.user
			new_review.restaurant = restaurant
			new_review.save()
			return HttpResponseRedirect(reverse('reviews:restaurant', args=(restaurant_id,)))

		else:
			print form.errors
	else:
		form = ReviewForm()

	return render(request, 'reviews/addreview.html', { 'form': form , 'restaurant': restaurant})

@csrf_protect
def fav_review(request, review_id, restaurant_id):
	review = get_object_or_404(Review, pk=review_id)
	restaurant = get_object_or_404(Restaurant, pk=restaurant_id)
	try:
		vote = Vote.objects.filter(restaurant=restaurant, review=review, created_by=request.user)[:1].get()
	except Vote.DoesNotExist:
			vote = None

	
	if request.method == 'POST':
		if vote == None:
			new_vote = Vote(restaurant=restaurant, review=review)
			new_vote.created_by = request.user
			review.upvotes += 1
			review.save()
			new_vote.save()
		else:
			review.upvotes -= 1
			review.save()
			vote.delete()
		all_reviews = get_reviews(request, restaurant)
		user_votes = all_reviews['user_votes']
		return render(request, 'reviews/review_vote_item.html', {
			'user_votes' : user_votes,
			'review' : review,
			'restaurant' : restaurant
			})
	else:
		return HttpResponseRedirect(reverse('reviews:restaurant', args=(restaurant_id,)))

@csrf_protect
def fav_restaurant(request, restaurant_id):
	restaurant = get_object_or_404(Restaurant, pk=restaurant_id)
	try:
		vote = Vote.objects.filter(restaurant=restaurant, review=None, created_by=request.user)[:1].get()
	except Vote.DoesNotExist:
		vote = None

	if request.method == 'POST':
		if vote == None:
			new_vote = Vote(restaurant=restaurant, review=None)
			new_vote.created_by = request.user
			restaurant.upvotes += 1
			restaurant.save()
			new_vote.save()
		else:
			restaurant.upvotes -= 1
			restaurant.save()
			vote.delete()
		user_votes = get_restaurant_user_votes(request, restaurant)
		return render(request, 'reviews/restaurant_vote_item.html', {
				'restaurant' : restaurant,
				'user_votes' : user_votes
			})
	else:
		return HttpResponseRedirect(reverse('reviews:index'))

def contact(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = "noshlocal feedback"
            from_email = form.cleaned_data['from_email']
            message = form.cleaned_data['message']
            try:
				msg = EmailMessage(subject,message, to=['info@noshlocal.com'])
				msg.send()
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return render(request, 'reviews/thanks.html')
    return render(request, "reviews/contact.html", {'form': form})

def thanks(request):
    return HttpResponse('Thank you for your feedback.')