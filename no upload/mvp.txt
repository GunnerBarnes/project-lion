Today, 5/2/15, I believe I have an MVP ready for beta testing. This project officially got underway on 2/6/15.
I've worked on it at a mostly leisurely pace; an hour or two in the morning before work, 2 or 3 days a week.
On weekends, I've probably put in 2-3 hours each day, though I certainly have worked less weekends than more.

The project is still unnamed. I dubbed it Project LION as in, "Like It Or Not". The general idea behind the application
is that finding a restaurant in an efficient manner is difficult if you're not familiar with an area. Yelp reviews are
personal manifestos huffing every little insignificant detail about someone's experience; it needs to be simpler. This 
site will allow users to enter a quick review, 140 characters max, about a restaurant. Reviews fall into 3 categories: 
good, average, bad. Users can upvote an existing review if they agree with it, or create their own. With a quick glance
at any restaurant, a user should be able to determine whether they should go there or not.

I have a lot of other ideas for this site; my hope is that I can bring it to the point where users can identify like-minded
(or the opposite) users to help them even more easily discover new restaurants they would enjoy. 

I don't know what else to say at this point. 

I need a name. 
